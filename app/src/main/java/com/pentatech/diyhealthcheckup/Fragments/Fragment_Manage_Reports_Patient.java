package com.pentatech.diyhealthcheckup.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pentatech.diyhealthcheckup.R;

/**
 * Created by Pasindu on 8/19/2017.
 */

public class Fragment_Manage_Reports_Patient extends Fragment implements View.OnClickListener{

    private static final String G_TAG = "Global"; //Debugging

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        Log.d(G_TAG, "Manage Report : Patient");
        return inflater.inflate(R.layout.fragment_manage_reports_patient, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Manage Your Reports");
    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_send:
                //Change the fragment to fragment_send_report
                new Fragment_Manage_Reports_Patient();
                break;
        }
    }
}
