package com.pentatech.diyhealthcheckup;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String G_TAG = "Global"; //Debugging

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(G_TAG, "onCreate : Main Activity");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*To test if the all the drawers are working : Clear shared preferences key value

        SharedPreferences sharedPreferences = getSharedPreferences("MyRole", Context.MODE_PRIVATE);
        sharedPreferences.edit().remove("Role").commit();

        */

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, ActivityNumberAuth.class);
                startActivity(intent);
            }

        }, 3000L);
    }
}
